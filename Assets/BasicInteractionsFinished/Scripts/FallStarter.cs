﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class FallStarter : MonoBehaviour
{

    [SerializeField]
    private float _startFallAfter = 0f;

    private Rigidbody _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    public void StartFall()
    {
        StartCoroutine(StartFallAfterSeconds(_startFallAfter));
    }

    private IEnumerator StartFallAfterSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        _rigidbody.useGravity = true;
    }

}
