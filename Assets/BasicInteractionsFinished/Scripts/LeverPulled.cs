﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR.InteractionSystem;


namespace BiFinished
{

    [RequireComponent(typeof(LinearMapping))]
    public class LeverPulled : MonoBehaviour
    {

        // Event which is called, when the lever is pulled to its end position.
        public UnityEvent OnLeverPulled;

        private LinearMapping _leverMapping;

        private void Awake()
        {
            _leverMapping = GetComponent<LinearMapping>();
        }

        private void Update()
        {
            // Check if the lever is pulled over a satisfactory amount.
            if (_leverMapping.value >= 0.95)
            {
                // Invoke the event.
                OnLeverPulled.Invoke();
                // Disable this script, so it doesn't invoke it forever.
                this.enabled = false;
            }
        }

    }

}